package homework3

fun main() {
    //yo-ho-ho!
    val treasureChest = arrayOf(1, 1, 10, 2, 3, 3, 5, 10)
    var captainsCoins = 0L // сколько монет получит капитан
    var teamsCoins = 0L // сколько монет получит команда
    for (coin in treasureChest) {
        if (coin%2 == 0) {
            println("Нам попалась монета с четным номиналом $coin. Джо забирает монету себе!")
            captainsCoins++
        } else {
            println("Нам попалась монета с нечетным номиналом $coin. Пусть она достанется команде!")
            teamsCoins++
        }
    }
    println("\nКоличество монет у капитана: $captainsCoins")
    println("Количество монет у команды: $teamsCoins")
}
package homework8

val userCart = mutableMapOf<String, Int>(
    "potato" to 2,
    "cereal" to 2,
    "milk" to 1,
    "sugar" to 3,
    "onion" to 1,
    "tomato" to 2,
    "cucumber" to 2,
    "bread" to 3
)
// овощи "potato" to 2, "onion" to 1, "tomato" to 2, "cucumber" to 2)


val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun main() {
    //call your functions here
    println("Количество всех овощей в корзине: ${howManyVegetables(userCart)}")
    println("Стоимость всех товаров в корзине: ${cartCost(userCart, discountSet, discountValue, prices)}")
}

//write your functions here
fun howManyVegetables(userCart: MutableMap<String, Int>): Int {
    var vegSum = 0
    userCart.forEach { sku ->
        if (sku.key in vegetableSet) vegSum += sku.value
    }
    return vegSum
}

fun cartCost(
    userCart: MutableMap<String, Int>,
    discountSet: Set<String>,
    discountValue: Double,
    prices: MutableMap<String, Double>
): Double {
    var sum = 0.0
    userCart.forEach { sku ->
        sum += sku.value * prices.getValue(sku.key) *
                (if (sku.key in discountSet) (1.0 - discountValue) else 1.0)
    }
    return sum

}
package homework8

fun main() {
    val sourceList = mutableListOf(1, 2, 3, 1, 2, 3, 1, 1, 5, 6, 8, 1, 8, 7, 4, 9)
    //call your function here
    val testList: Set<Int>
    testList = deleteDublicates(sourceList)
    println("Удаляем дубликаты: $testList")
    println("Вот можно еще отсортировать, чтобы убедиться что дубликатов действительно нет: ${testList.sorted()}")
}

//write your code here
fun deleteDublicates(sourceList: MutableList<Int>): Set<Int> {
    return sourceList.toMutableList().toSet()
}
package homework5

fun main() {
    val digit = readLine()!!.toInt()
    //call written function here
    println("Перевернутое число = ${flipNumber(digit)}")
}

//write your function here
fun flipNumber(digit: Int): Int {
    var inputNumber: Int
    var outputNumber = 0
    var tmp: Int
    var sign = 1
    var currentPosition = 1
    var i: Byte = 0

    if (digit < 0) {
        sign = -1
        inputNumber = -digit
    } else {
        inputNumber = digit
    }

    tmp = inputNumber
    do { // вычислим количество разрядов числа
        currentPosition = currentPosition * 10
        tmp /= 10
    } while (tmp !== 0)

    outputNumber = outputNumber + (inputNumber % 10) * currentPosition / 10 // это мы пока нашли 1-ю цифру перевернутого числа
    println("Сначала outputNumber = $outputNumber")
    tmp = inputNumber
    while (tmp > 0) {
        i++
        currentPosition /= 10
        tmp /= 10
        println("($tmp % 10) = ${(tmp % (i * 10))}")
        outputNumber = outputNumber + tmp % 10 * currentPosition / 10
        println("И теперь outputNumber = $outputNumber")
    }
    return sign * outputNumber
}

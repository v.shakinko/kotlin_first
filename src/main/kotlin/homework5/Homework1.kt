package homework5

//write your classes here
//у всех классов объявлены все 5 свойств (через конструктор 3 и 2 просто)

fun main() {
    val myLeon = Leon("Chandra lion", 100, 100)
    val myTiger = Tiger("Shere Khan tiger", 110, 150)
    val myHippopotamus = Hippopotamus("Max hippopotamus", 120, 300)
    val myWolf = Wolf("White Fang wolf", 90, 90)
    val myGiraffe = Giraffe("Samson giraffe", 500, 700)
    val myElephant = Elephant("Benjamin elephant", 300, 800)
    val myChimpanzee = Chimpanzee("Washoe chimpanzee", 80, 70)
    val myGorilla = Gorilla("Gorilka gorilla", 140, 200)

    myLeon.eat("antelope meat") // покормим льва
    myLeon.eat("bison meat") // покормим льва еще раз (для теста)
    myTiger.eat("milk") // покормим тигра
    myHippopotamus.eat("fish") // покормим бегемота
    myWolf.eat("123") // покормим волка
    myGiraffe.eat("leaves") // покормим жирафа
    myElephant.eat("fruits") // покормим слона
    myChimpanzee.eat("berries") // покормим шимпанзе
    myGorilla.eat("money") // покормим гориллу
}

class Leon(val name: String, val height: Int, val weight: Int) {
    val foodPreferences = arrayOf("antelope meat", "bison meat", "saiga meat")
    var satiety: Int = 0
    fun eat(food: String) {
        if (food in foodPreferences) satiety++
        println("$name: my satiety is $satiety")
    }
}

class Tiger(val name: String, val height: Int, val weight: Int) {
    val foodPreferences = arrayOf("fish", "milk", "chicken")
    var satiety: Int = 0
    fun eat(food: String) {
        if (food in foodPreferences) satiety++
        println("$name: my satiety is $satiety")
    }
}

class Hippopotamus(val name: String, val height: Int, val weight: Int) {
    val foodPreferences = arrayOf("grass", "carrion", "hay")
    var satiety: Int = 0
    fun eat(food: String) {
        if (food in foodPreferences) satiety++
        println("$name: my satiety is $satiety")
    }
}

class Wolf(val name: String, val height: Int, val weight: Int) {
    val foodPreferences = arrayOf("hare meat", "sausage", "legs")
    var satiety: Int = 0
    fun eat(food: String) {
        if (food in foodPreferences) satiety++
        println("$name: my satiety is $satiety")
    }
}

class Giraffe(val name: String, val height: Int, val weight: Int) {
    val foodPreferences = arrayOf("leaves", "oats", "potatoes")
    var satiety: Int = 0
    fun eat(food: String) {
        if (food in foodPreferences) satiety++
        println("$name: my satiety is $satiety")
    }
}

class Elephant(val name: String, val height: Int, val weight: Int) {
    val foodPreferences = arrayOf("fruits", "tree bark", "small twigs")
    var satiety: Int = 0
    fun eat(food: String) {
        if (food in foodPreferences) satiety++
        println("$name: my satiety is $satiety")
    }
}

class Chimpanzee(val name: String, val height: Int, val weight: Int) {
    val foodPreferences = arrayOf("cereals", "berries", "insects")
    var satiety: Int = 0
    fun eat(food: String) {
        if (food in foodPreferences) satiety++
        println("$name: my satiety is $satiety")
    }
}

class Gorilla(val name: String, val height: Int, val weight: Int) {
    val foodPreferences = arrayOf("celery", "blue pygeum fruits", "nuts")
    var satiety: Int = 0
    fun eat(food: String) {
        if (food in foodPreferences) satiety++
        println("$name: my satiety is $satiety")
    }
}
package final_task

fun main() {

    print("Введите имя первого игрока: ")
    val firstPlayerName = readln()
    val Player1 = Player(firstPlayerName)
    Player1.gameFieldFilling()
    println("$firstPlayerName, вот расположение ваших кораблей: ")
    Player1.gameFieldOutput()

    var secondPlayerName: String
    while (true) {
        print("Введите имя второго игрока: ")
        secondPlayerName = readln()
        if (secondPlayerName != firstPlayerName) {
            break
        } else {
            println("Вы ввели некорректное значение. Имя второго игрока не может быть таким же как у первого игрока")
        }
    }

    val Player2 = Player(secondPlayerName)
    Player2.gameFieldFilling()
    println("$secondPlayerName, вот расположение ваших кораблей: ")
    Player2.gameFieldOutput()

// TODO: здесь цикл нужен
    Player1.shoot()


}


class Ship(val name: String) {
    var x = 0
    var y = 0
    var size = 1
    var direction = "-"
    fun isAlive(board: Array<Array<Int>>): Boolean {
        var possiblyDestroyed = false
        if (board[x][y] == 2) {
            if ((direction == "-") && (board[x + 1][y] == 2)) possiblyDestroyed = true
            if ((direction == "|") && (board[x][y + 1] == 2)) possiblyDestroyed = true
        }
        return !possiblyDestroyed
    }
}

class Player(val name: String) {
    val occupiedFields: Array<Array<Int>> = Array(4, { Array(4, { 0 }) })
    val opponentsFields: Array<Array<Int>> = Array(4, { Array(4, { 0 }) })

    val shipChar = "⬛"
    val emptyChar = "⬜"
    val missedChar = "▣ "
    val damagedChar = "\uD83D\uDFE5"

    var twoDeskShipDirection = "-"

    var ships: Array<Ship> = arrayOf(
        Ship("2 desk ship Aurora"),
        Ship("1 desk ship Bystryj"),
        Ship("1 desk ship Zloj")
    )

    fun gameFieldFilling() {
        var x: Int
        var y: Int
        println("$name, у вас есть игровое поле 4X4. Давайте заполним его кораблями.")
        println("Ваш флот это 1 двухпалубный корабль и 2 однопалубных. Начнем с 2-палубного.")
        while (true) {
            println("У нас есть 4 ряда. В каком ряду игрового поля будет ваш двухпалубный корабль? Введите число от 0 до 3:")
            y = readln().toInt()
            if (y in 0..3) {
                break
            } else {
                println("Вы ввели некорректное значение")
            }
        }
        if (y != 3) {
            while (true) {
                println("И давайте еще зададим направление двухпалубного корабля. Для горизонтального направления введите \"-\", для вертикального введите \"|\"")
                twoDeskShipDirection = readln()
                if (twoDeskShipDirection == "-" || twoDeskShipDirection == "|") {
                    break
                } else {
                    println("Вы ввели некорректное значение. Введите \"-\" или \"|\"")
                }
            }
        } else {
            println("Вы ввели число $y. Это номер последнего ряда игрового поля. Значит, ваш двухпалубник может быть расположен только горизонтально")
            twoDeskShipDirection = "-"
        }
        while (true) {
            println("У нас есть 4 столбца. В каком столбце игрового поля будет начинаться ваш двухпалубный корабль? Введите число от 0 до 3:")
            x = readln().toInt()
            if (twoDeskShipDirection == "-" && x in 0..2) {
                occupiedFields[x + 1][y] = 1
                break
            } else if (twoDeskShipDirection == "|" && x in 0..3) {
                occupiedFields[x][y + 1] = 1
                break
            } else {
                println("Вы ввели некорректное значение")
            }
        }
        occupiedFields[x][y] = 1
        ships[0].direction = twoDeskShipDirection
        ships[0].x = x
        ships[0].y = y
        ships[0].size = 2

        while (true) {
            println("Давайте расположим однопалубные корабли. Введите номер ряда от 0 до 3:")
            y = readln().toInt()
            if (y in 0..3) {
                break
            } else {
                println("Вы ввели некорректное значение")
            }
        }
        while (true) {
            println("Теперь укажем в каком столбце будет ваш однопалубный корабль. Введите число от 0 до 3:")
            x = readln().toInt()
            if ((x in 0..3) && (occupiedFields[x][y] == 0)) {
                occupiedFields[x][y] = 1
                break
            } else {
                println("Вы ввели некорректное значение. Поле ($x, $y) уже занято, либо вы ввели не число от 0 до 3.")
            }
        }
        ships[1].x = x
        ships[1].y = y
        while (true) {
            println("И наконец последний однопалубный корабль. В каком он ряду? Введите число от 0 до 3:")
            y = readln().toInt()
            if (y in 0..3) {
                break
            } else {
                println("Вы ввели некорректное значение")
            }
        }
        while (true) {
            println("Укажем в каком столбце ваш последний однопалубный корабль. Введите число от 0 до 3:")
            x = readln().toInt()
            if ((x in 0..3) && (occupiedFields[x][y] == 0)) {
                occupiedFields[x][y] = 1
                break
            } else {
                println("Вы ввели некорректное значение. Поле ($x, $y) уже занято, либо вы ввели не число от 0 до 3.")
            }
            ships[1].x = x
            ships[1].y = y
        }
    }

    fun gameFieldOutput() {
        var x: Int = 0
        var y: Int = 0
        for (y in 0..3) {
            for (x in 0..3) {
                when {
                    occupiedFields[x][y] == 0 -> print(emptyChar)
                    occupiedFields[x][y] == 1 -> print(shipChar)
                    occupiedFields[x][y] == 2 -> print(damagedChar)
                }
            }
            println()
        }
    }

    fun shoot() {
        var x: Int = 0
        var y: Int = 0
        var x_it: Int = 0
        var y_it: Int = 0
        println("$name, стреляйте! Введите координаты выстрела. Сначала номер столбца от 0 до 3, затем номер ряда от 0 до 3")
        while (true) {
            x = readln().toInt()
            y = readln().toInt()
            if (x in 0..3 && y in 0..3) {
                println("Бах!!!")
                opponentsFields[x][y] = 3
                for (y_it in 0..3) {
                    for (x_it in 0..3) {
                        when {
                            opponentsFields[x_it][y_it] == 0 -> print(emptyChar)
                            opponentsFields[x_it][y_it] == 3 -> print(missedChar)
                            opponentsFields[x_it][y_it] == 2 -> print(damagedChar)
                        }
                    }
                    println()
                }
                break
            } else {
                println("Вы ввели ($x, $y). Проверьте, существуют ли такие координаты. Будьте внимательны")
            }
        }
    }
}


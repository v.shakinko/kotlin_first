package homework6

//write you classes here

//TODO: Homework2 write your function here
fun feedAll(
    animals: Array<Animal>,
    foodArray: Array<String>
) {
    animals.forEach {
        foodArray.forEach { food ->
            it.eat(food)
        }
    }
}

fun main() {
    val animals: Array<Animal> = arrayOf(
        Leon("Chandra lion", 100, 100),
        Tiger("Shere Khan tiger", 110, 150),
        Hippopotamus("Max hippopotamus", 120, 300),
        Wolf("White Fang wolf", 90, 90),
        Giraffe("Samson giraffe", 500, 700),
        Elephant("Benjamin elephant", 300, 800),
        Chimpanzee("Washoe chimpanzee", 80, 70),
        Gorilla("Gorilka gorilla", 140, 200)
    )
    val foodArray: Array<String> = arrayOf("fish", "milk")
    feedAll(animals, foodArray)
}

abstract class Animal(open val name: String, open val height: Int, open val weight: Int) {
    abstract val foodPreferences: Array<String>
    abstract val color: String
    private var satiety: Int = 0
    open fun eat(food: String) {
        if (food in foodPreferences) satiety++
        println("$name: let's try some $food, my satiety is $satiety")
    }
}

class Leon(override val name: String, override val height: Int, override val weight: Int) :
    Animal(name, height, weight) {
    override val foodPreferences = arrayOf("antelope meat", "bison meat", "saiga meat")
    override val color = "yellow"
}

class Tiger(override val name: String, override val height: Int, override val weight: Int) :
    Animal(name, height, weight) {
    override val foodPreferences = arrayOf("fish", "milk", "chicken")
    override val color = "striped"
}

class Hippopotamus(override val name: String, override val height: Int, override val weight: Int) :
    Animal(name, height, weight) {
    override val foodPreferences = arrayOf("grass", "carrion", "hay")
    override val color = "grey"
}

class Wolf(override val name: String, override val height: Int, override val weight: Int) :
    Animal(name, height, weight) {
    override val foodPreferences = arrayOf("hare meat", "fish", "legs")
    override val color = "white"
}

class Giraffe(override val name: String, override val height: Int, override val weight: Int) :
    Animal(name, height, weight) {
    override val foodPreferences = arrayOf("leaves", "oats", "potatoes")
    override val color = "spotted"
}

class Elephant(override val name: String, override val height: Int, override val weight: Int) :
    Animal(name, height, weight) {
    override val foodPreferences = arrayOf("fruits", "tree bark", "small twigs")
    override val color = "pink"
}

class Chimpanzee(override val name: String, override val height: Int, override val weight: Int) :
    Animal(name, height, weight) {
    override val foodPreferences = arrayOf("milk", "berries", "insects")
    override val color = "black"
}

class Gorilla(override val name: String, override val height: Int, override val weight: Int) :
    Animal(name, height, weight) {
    override val foodPreferences = arrayOf("celery", "blue pygeum fruits", "nuts")
    override val color = "hoary"
}


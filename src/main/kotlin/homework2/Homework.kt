package homework2
fun main() {
    val IlyaLemonade = 18500
    val IlyaPinaColada: Short = 200
    val IlyaWhiskey: Byte = 50
    val IlyaFresh = 3000000000L
    val IlyaCola = 0.5F
    val IlyaAle = 0.666666667
    val IlyaCraft = "Что-то авторское!"
    println("Заказ - ‘$IlyaLemonade мл лимонада’ готов!")
    println("Заказ - ‘$IlyaPinaColada мл пина колады’ готов!")
    println("Заказ - ‘$IlyaWhiskey мл виски’ готов!")
    println("Заказ - ‘$IlyaFresh капель фреша’ готов!")
    println("Заказ - ‘$IlyaCola литра колы’ готов!")
    println("Заказ - ‘$IlyaAle литра эля’ готов!")
    println("Заказ - ‘$IlyaCraft’ готов!")
}



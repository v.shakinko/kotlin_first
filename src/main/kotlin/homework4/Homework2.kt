package homework4

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)
    var aStudents: Byte = 0 // количество отличников
    var bStudents: Byte = 0 // количество хорошистов
    var cStudents: Byte = 0 // количество троечников
    var dStudents: Byte = 0 // количество двоечников
    var percentage: Float = 0.0F // для вычисления процента
    for (mark in marks) {
        when (mark) {
            2 -> dStudents++
            3 -> cStudents++
            4 -> bStudents++
            5 -> aStudents++
        }
    }

    percentage = 100*aStudents.toFloat()/marks.size
    println("Отличников - $percentage%")
    percentage = 100*bStudents.toFloat()/marks.size
    println("Хорошистов - $percentage%")
    percentage = 100*cStudents.toFloat()/marks.size
    println("Троечников - $percentage%")
    percentage = 100*dStudents.toFloat()/marks.size
    println("Двоечников - $percentage%")
}
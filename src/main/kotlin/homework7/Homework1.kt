package homework7

fun main() {
    print("Логин:")
    val userLogin = readln()
    print("Пароль:")
    val userPswd = readln()
    print("Подтвердите пароль:")
    val pswdConfirm = readln()

    val newUser = createUser(userLogin, userPswd, pswdConfirm)
    println("Создаем ${newUser.login}")
}

//write your function and Exception-classes here
fun createUser(userLogin: String, userPswd: String, pswdConfirm: String): User {
    if(userLogin.isEmpty() or (userLogin.length > 20)) {
        throw WrongLoginException("Разрешаются логины от 1 до 20 символов!")
    }
    if(userPswd.isEmpty() or (userPswd.length < 10)) {
        throw WrongPasswordException("В пароле не должно быть менее 10 символов!")
    }
    if(userPswd != pswdConfirm) {
        throw WrongPasswordException("Пароль и подтверждение пароля должны совпадать!")
    }
    return User(userLogin)
}

class User(val login: String)

class WrongLoginException(message: String = "WrongLoginException"): Exception(message)

class WrongPasswordException(message: String = "WrongPasswordException"): Exception(message)

